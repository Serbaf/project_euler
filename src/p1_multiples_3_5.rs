use std::io;

fn main() {
    let mut n_entries: String = String::new();
    io::stdin().read_line(&mut n_entries).expect("Invalid input");
    let n_entries: i32 = n_entries.trim().parse().unwrap();

    // The the N inputs to compute
    let mut input_vec = Vec::new();
    for _i in 0..n_entries
    {
        let mut m: String = String::new();
        io::stdin().read_line(&mut m).expect("Invalid input");
        let m: i32 = m.trim().parse().unwrap();
        input_vec.push(m);
    }

    // For each of that numbers
    for _i in 0..n_entries
    {
        let m: i64 = input_vec[0] as i64;
        input_vec.remove(0);
        let total: i64 = 
            (0..((m - 1)/3 + 1)).map(|x| x * 3).sum::<i64>() + 
            (0..((m - 1)/5 + 1)).map(|x| x * 5).sum::<i64>() - 
            (0..((m - 1)/15 + 1)).map(|x| x * 15).sum::<i64>();
        println!("{}", total);
    }
}
